<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BindingServiceProvider extends ServiceProvider {
	
	/**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register() {
    	// User service
        $this->app->bind('App\Models\Contracts\UserInterface', 'App\Models\Services\UserService');    
    }
}