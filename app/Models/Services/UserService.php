<?php namespace App\Models\Listing\Services;

use App\Models\Contracts\UserInterface;
use App\User;

class UserService implements UserInterface {

	public function add($data){
		if($data) {			
			$user = new User($data);
			$user->save();			
			return $user;
		}
		return null;
	}
	public function update($data, $id){
		if($data) {
			$user = User::find($id);		
			$user->update($data);
			return $user;
		}
		return null;
	}
	public function delete(User $user){
        return $user->delete();
    }
	
	public function all($data){
		return User::all();
	}

	public function get($data){}	

}