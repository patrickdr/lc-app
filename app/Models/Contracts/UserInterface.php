<?php namespace App\Models\Contracts;

interface UserInterface {	
	public function add($data);
	public function update($data, $id);
	public function delete(User $user);
	public function all($data);
	public function get($data);
}